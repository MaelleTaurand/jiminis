package sio.slam2;

/**
 * Classe Boîte
 */
public class Boite {
	private String assaisonnement;
	private double prix;
	private int poids;//en gramme
	private Insecte linsecte;

	/**
	 * Constructeur
	 * TODO 1 : Compléter la signature et le code du constructeur
	 */
	public Boite(String unAssai, double prix,int poids, Insecte linsecte) {
		this.assaisonnement=unAssai;
		this.prix = prix;
		this.poids = poids;
		this.linsecte = linsecte;

	}

	/**
	 * Getter sur poids
	 * @return
	 */
	public int getPoids() {
		return poids;
	}

	/**
	 * Getter sur linsecte
	 * @return
	 */
	public Insecte getLinsecte() {
		return linsecte;
	}

	/**
	 * Getter sur assaisonnement
	 * @return
	 */
	public String getAssaisonnement() {
		return assaisonnement;
	}

	/**
	 * Retourne le prix au kilo
	 * @return
	 */
	public double prixAuKilo(){
		//TODO 4 : implémenter la méthode
		return (this.prix/this.poids)*1000;
	}
	
}
