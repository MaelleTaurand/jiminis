package sio.slam2;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Classe Catalogue
 */
public class Catalogue {
	private ArrayList<Boite> lesBoites; // liste des boîtes d'insecte

	/**
	 * Constructeur
	 */
	public Catalogue() {
		this.lesBoites = new ArrayList<Boite>();
	}

	/**
	 * Permet d'ajouter une boite. Retourne True si l'ajout s'est bien déroulé, false sinon
	 *
	 * @param uneBoite
	 */
	public boolean ajouterUneBoite(Boite uneBoite) {
		//TODO 2 : implémenter la méthode
		boolean res;
		res = this.lesBoites.add(uneBoite);
		return res;
	}

	/**
	 * Retourne le nombre de boites contenues dans le catalogue
	 *
	 * @return
	 */
	public int nbBoites() {
		//TODO 3 : implémenter la méthode

		return this.lesBoites.size();
	}

	/**
	 * Retourne la boîte la plus chère au kilo
	 *
	 * @return
	 */
	public Boite laBoiteLaPlusChereAuKilo() {
		//TODO 5 : implémenter la méthode

		double max = 0;
		Boite boiteMax = this.lesBoites.get(0);
		Boite uneBoite;

		for (int i = 0; i < this.lesBoites.size(); i++) {

			uneBoite = this.lesBoites.get(i);

			if (uneBoite.prixAuKilo() > boiteMax.prixAuKilo()) {
				boiteMax = uneBoite;
			}
		}
		return boiteMax;
	}

	/**
	 * Retourne le poids moyen des boites présentes dans le catalogue
	 *
	 * @return
	 */
	public double poidsMoyenDesBoites() {
		Boite uneBoite;
		double res = 0;
		int totalpoids = 0;

		if (nbBoites() != 0) {

			for (int i = 0; i < this.lesBoites.size(); i++) {
				uneBoite = this.lesBoites.get(i);
				totalpoids = totalpoids + uneBoite.getPoids();
			}
			res = totalpoids / this.nbBoites();
		}

		return res;
	}

	/**
	 * Permet de vider la liste des boîtes
	 */
	public void vider() {
		this.lesBoites.clear();
	}

	/**
	 * Retourner la liste des boîtes correspondant à l'insecte recherché
	 *
	 * @param nomInsecte
	 * @return
	 */
	public ArrayList<Boite> lesBoitesDepuisNomInsecte(String nomInsecte) {

		ArrayList<Boite> reslesBoites = new ArrayList<Boite>();

		// on parcourt la liste les boites
		// lorsqu'on rencontre une boite correspondant à l'insecte recherché on l'ajoute à notre res

		for (Boite uneBoite : this.lesBoites) {

			String nomIc = uneBoite.getLinsecte().getNom();
			if (nomIc.equals(nomInsecte)) { //nomIc == nomInsecte
				reslesBoites.add(uneBoite);
			}
		}

		return reslesBoites;

	}

	/**
	 * Retourner une chaine de caractères contenant la liste des assaissonnements pour un insecte donné
	 * @param nom : au nom de l'insecte recherché
	 * @return
	 */
	public String lesAssaisonnementsByNomInsecte(String nom){
		String res = "Liste des assaisonnements pour le "+ nom;

		for (Boite uneboite:this.lesBoites
			 ) {
			if (uneboite.getLinsecte().getNom().equals(nom)){
				//c'est le bon insecte
				String assai = uneboite.getAssaisonnement();
				res = res + " - " + assai;
			}
		}
		return res;
	}

}