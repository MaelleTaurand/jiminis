package sio.slam2;

public class Main {

    public static void main(String[] args) {

        Insecte c = new Insecte("criquet",28);
        Insecte g = new Insecte("grillon",25);
        Insecte m = new Insecte("molitor",28);

        Catalogue cat = new Catalogue();

        // création des boites
        cat.ajouterUneBoite(new Boite("mangue douce",7.9,14,g));
        cat.ajouterUneBoite(new Boite("oignon fumé",7.9,14,g));
        cat.ajouterUneBoite(new Boite("paprika",6.9,10,c));
        cat.ajouterUneBoite(new Boite("poivre & tomates séchées",6.9,10,c));
        cat.ajouterUneBoite(new Boite("curry fruité",6.9,10,c));
        cat.ajouterUneBoite(new Boite("à la grecque",6.9,10,c));
        cat.ajouterUneBoite(new Boite("sésame & cumin",6.9,18,m));
        cat.ajouterUneBoite(new Boite("ail & fines herbes",6.9,18,m));
        cat.ajouterUneBoite(new Boite("soja impérial",6.9,18,m));

        assert cat.nbBoites() == 9 : "Pb sur le nombre de boites";

        Boite uneB = new Boite("soja impérial",6.9,18,m);
        assert (int)uneB.prixAuKilo() == 383 : "prix au kilo";

        // Test boite la plus chère au kilo
        Boite b = cat.laBoiteLaPlusChereAuKilo();
        assert b.getLinsecte().getNom().equals("criquet") : "Pb sur boite la plus chère au kilo";
        assert b.getAssaisonnement().equals("paprika"):"Pb sur boite la plus chère au kilo";

        Double res=cat.poidsMoyenDesBoites();
        assert res == (14+14+10+10+10+10+18+18+18)/9 : "Pb de poids moyen";

        //cat.vider();
        //assert cat.nbBoites()==0;
        //res = cat.poidsMoyenDesBoites();
        //assert res == 0 : "Pb poids moyen catalogue vide";

        // test lesBoitesNomInsecte
        assert cat.lesBoitesDepuisNomInsecte("criquet").size() == 4 : "Pb sur la recherche de criquet";
        assert cat.lesBoitesDepuisNomInsecte("sauterelle").size() == 0 : "Pb sur la recherche d'insecte non présent";

        assert cat.lesAssaisonnementsByNomInsecte("criquet").equals("Liste des assaisonnements pour le criquet - paprika - poivre & tomates séchées - curry fruité - à la grecque") : "Pb assaisonnements";
        assert cat.lesAssaisonnementsByNomInsecte("grillon").equals("Liste des assaisonnements pour le grillon - mangue douce - oignon fumé") : "Pb assaisonnements";

    }

}
