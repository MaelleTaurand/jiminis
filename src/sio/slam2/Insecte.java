package sio.slam2;

/**
 * Classe Insecte
 */
public class Insecte {
	private String nom;
	private int rnj;//repere nutritionnel journalier

	/**
	 * Constructeur
	 * @param nom
	 * @param rnj
	 */
	public Insecte(String nom, int rnj) {
		this.nom = nom;
		this.rnj = rnj;
	}

	/**
	 * Getter sur le nom
	 * @return
	 */
	public String getNom() {
		return this.nom;
	}
}
